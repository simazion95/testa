@extends('layouts.app')
@section('content')
<h1>Create new customers</h1>
<form method='post' action="{{action('CustomerController@store')}}">
    {{csrf_field()}}
    <div class="form-group">
        <label for ="name">name</label>
        <input type="text" class= "form-control" name="name">

        <label for ="phone">phone number</label>
        <input type="text" class= "form-control" name="phone">

        <label for ="email">email</label>
        <input type="text" class= "form-control" name="email">
    </div>
    <div class = "form-group">
        <input type="submit" class="form-control" name="submit" value="save">
    </div>
</form>
@endsection