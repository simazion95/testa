@extends('layouts.app')
@section('content')

<h1>customers list</h1>

<table >
<tr>
   <th></th>
    <th>name</th>
    <th>email</th>
    <th>phone</th>
    <th>user name</th>
    <th></th>
    @cannot('salesrep')
    <th></th>
    @endcannot
   
</tr>
    @foreach($customers as $customer)
    <tr>
        @if ($customer->status)
            <td></td>
            @if($customer->user_id==Auth::id())
                 <td style="color:green;font-weight:bold">{{$customer->name}}</td>
            @else
                <td style="color:green">{{$customer->name}}</td>
            @endif
        @else
        
        <td>@cannot('salesrep')<a href="{{route('status_update', [ $customer->id])}}">deal closed</a>   @endcannot</td>
      
            @if($customer->user_id==Auth::id())
                <td style="font-weight:bold">{{$customer->name}}</td>
            @else
                <td>{{$customer->name}}</td> 
            @endif
        @endif

        
        <td>{{$customer->email}}</td>
        <td>{{$customer->phone}}</td>
        <td>{{$customer->user_name}}</td>
        <td><a href = "{{route('customers.edit', $customer->id)}}"> edit </a></td> 
        @cannot('salesrep')
        <td> <form method = 'post' action = "{{action('CustomerController@destroy', $customer->id)}}" >
                @csrf
                @method ('DELETE')
                <div class = "form-group">
                    <input type = "submit" class = "form-control" name = "submit" value = "Delete">
                </div>
            </form>
        </td>
        @endcannot
    </tr>
 @endforeach
</table>
<a href = "{{route('customers.create')}}"> Create a new Customer</a>
@endsection