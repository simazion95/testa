@extends('layouts.app')
@section('content')

<form method = 'post' action="{{action('CustomerController@update', $customer->id)}}">
@csrf
@method('PATCH')
<div class = "form-group">
    <label for = "name">Edit name</label>
    <input type= "text" class = "form-control" name= "name" value = "{{$customer->name}}">
</div>
<div class = "form-group">
    <label for = "name">Edit phone</label>
    <input type= "text" class = "form-control" name= "phone" value = "{{$customer->phone}}">
</div>
<div class = "form-group">
    <label for = "name">Edit email</label>
    <input type= "text" class = "form-control" name= "email" value = "{{$customer->email}}">
</div>

<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Just Do It">
</div>

@endsection