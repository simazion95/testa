<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Customer;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::id();
      $customers=Customer::all();
      $todos=User::find($id)->customers;
      return view('customers.index',['customers'=>$customers]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customer = new Customer();
        $id=Auth::id();
        $user = User::Find($id);
        $customer->name = $request->name;
        $customer->user_id=$id;
        $customer->email = $request->email;
        $customer->phone = $request->phone;
        $customer->user_name = $user->name;
        $customer->save();
        return redirect('customers');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user_id=Auth::id();
        $customer = Customer::find($id);
        if (Gate::denies('manager')) {

            if($customer->user_id!=$user_id){
                abort(403,"sorry,You can not edit customers");
            }
       }
        return view('customers.edit',['customer'=>$customer]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $customer = Customer::findOrFail($id);
       
        $customer = Customer::findOrFail($id);
        $customer -> update($request->except(['_token']));
        return redirect('customers'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('manager')) {
            abort(403,"you are not allowed to delete customers");
       }
        $customer=Customer::find($id);
        $customer->delete();
        return redirect ('customers');
    }
    public function status_update(Request $request, $id)
       {
        if (Gate::denies('manager')) {
            abort(403,"sorry,You can not update customers");
        }
            $customer = Customer::find($id);
            $customer->status = 1; 
            $customer->update($request -> all());
            return redirect('customers');
       }
}
